package com.kubrynski.tower.web;

import com.kubrynski.tower.model.Forecast;
import com.kubrynski.tower.service.ForecastService;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Jakub Kubrynski
 */
@RestController
@RequestMapping("/forecast")
class ForecastController {

	private final ForecastService forecastService;

	ForecastController(final ForecastService forecastService) {
		this.forecastService = forecastService;
	}

	@GetMapping
	Forecast checkDecision() {
		return forecastService.currentForecast();
	}
}
