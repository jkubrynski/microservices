package com.kubrynski.tower.service;


import com.kubrynski.tower.model.Condition;
import com.kubrynski.tower.model.Forecast;

import org.springframework.stereotype.Service;

/**
 * @author Jakub Kubrynski
 */
@Service
public class ForecastService {

	public Forecast currentForecast() {
		return new Forecast(Condition.GOOD);
	}
}
