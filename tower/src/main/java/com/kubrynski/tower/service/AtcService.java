package com.kubrynski.tower.service;

import com.kubrynski.tower.model.Decision;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * @author Jakub Kubrynski
 */
@Service
public class AtcService {

	private final RestTemplate restTemplate;

	public AtcService(final RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	public Decision decide() {
		ResponseEntity<Forecast> forecastResponse = restTemplate.getForEntity("http://localhost:8085/forecast", Forecast.class);
		if (forecastResponse.getStatusCode() != HttpStatus.OK) {
			return Decision.unknown();
		} else {
			Condition condition = forecastResponse.getBody().getCondition();
			return condition == Condition.GOOD ? Decision.land() : Decision.hold();
		}
	}
}
