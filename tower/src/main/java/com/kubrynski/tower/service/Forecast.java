package com.kubrynski.tower.service;

/**
 * @author Jakub Kubrynski
 */
class Forecast {

	private Condition condition;

	void setCondition(final Condition condition) {
		this.condition = condition;
	}

	Condition getCondition() {
		return condition;
	}
}
