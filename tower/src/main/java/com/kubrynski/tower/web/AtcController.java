package com.kubrynski.tower.web;

import com.kubrynski.tower.model.Decision;
import com.kubrynski.tower.service.AtcService;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Jakub Kubrynski
 */
@RestController
@RequestMapping("/atc")
class AtcController {

	private final AtcService atcService;

	AtcController(final AtcService atcService) {
		this.atcService = atcService;
	}

	@GetMapping
	Decision checkDecision() {
		return atcService.decide();
	}
}
